import React from 'react';
import {ScrollView,Image,StyleSheet, TouchableOpacity} from 'react-native';

const suka = (props) => {
    return (
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onButtonPress}>
            <Image source={require('../assets/like.png')} style={styles.love} />
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/komentar.png')} style={styles.love} />
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/bagikan.png')} style={styles.love} />
            </TouchableOpacity>
            <TouchableOpacity>
            <Image source={require('../assets/simpan.png')} style={{width:27,height:25,marginTop:10,marginLeft:210}} />
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    love : {
        width : 29,
        height : 27,
        marginLeft : 10,
        marginTop : 10
    }
});

export default suka;